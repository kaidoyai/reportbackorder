<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;


class qrt extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $users = DB::select("select itemid,remainsalesphysical,salesunit from SALESLINE 
        //     where remainsalesphysical > 0 and salesid like 'BO%'
        //     ORDER BY remainsalesphysical DESC");
        
        $users = DB::table('salesline')
        ->join('inventtable', 'salesline.itemid', '=', 'inventtable.itemid')
        ->where('inventtable.dataareaid','=','wor')
        ->where('salesline.salesid','like','BO%')
        ->where('salesline.remainsalesphysical','>',0)
        ->where('salesline.lineamount','>',3000) 
        ->where('salesline.dataareaid','=','wor')      
        ->select('salesline.itemid','salesline.name',DB::raw('SUM(salesline.remainsalesphysical) as qty'),
        DB::raw('SUM(salesline.lineamount) as amt'),'salesline.salesunit','inventtable.date_stock_in','inventtable.mustcancel')  
        ->groupBy('salesline.itemid')
        ->groupBy('salesline.name')
        ->groupBy('salesline.salesunit')
        ->groupBy('inventtable.date_stock_in')
        ->groupBy('inventtable.mustcancel')
        ->get();

        return view('reportb.backorder', ['users' => $users]);
    }

    public function index1()
    {
        $users = DB::table('salesline')
        ->join('inventtable', 'salesline.itemid', '=', 'inventtable.itemid')
        ->join('salestable','salesline.salesid','=','salestable.salesid')
        ->join('custtable','salesline.custaccount','=','custtable.accountnum')
        ->where('custtable.dataareaid','wor')
        ->where('salestable.dataareaid','wor')
        ->where('salestable.salesstatus',1)
        ->where('salestable.statussales',1)
        ->where('salestable.status_print',0)
        ->where('salestable.confirmcrm',1)
        ->where('inventtable.dataareaid','wor')
        ->where('inventtable.itemgroupid','like','FG_top%')
        ->where('salesline.salesid','like','SO%')
        ->where('salesline.remainsalesphysical','>',0)
        ->where('salesline.lineamount','>',3000) 
        ->where('salesline.dataareaid','wor')      
        ->select('salesline.salesid','salesline.itemid','salesline.name','salesline.remainsalesphysical','salesline.lineamount',
        'salesline.salesunit','inventtable.date_stock_in','salestable.custaccount','salestable.salesname',
        'custtable.salesdistrictid')  
        ->get();

        return view('reportb.ordertopline', ['users' => $users]);

    }
}
